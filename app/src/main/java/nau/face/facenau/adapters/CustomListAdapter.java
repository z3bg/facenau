package nau.face.facenau.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import nau.face.facenau.R;

public class CustomListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] itemname;
    private final Integer[] imgid;

/*
    @BindView(R.id.mynau_button)
    protected View mMynauView;
*/


    public CustomListAdapter(Activity context, String[] itemname, Integer[] imgid) {
        super(context, R.layout.mylist, itemname);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.itemname=itemname;
        this.imgid=imgid;
    }

    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.mylist, null,true);

//        View mynauView = (View) rowView.findViewById(R.id.mynau_button);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.item);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        TextView txtCoins = (TextView) rowView.findViewById(R.id.coins_text);
        ImageView imageCoinsView = (ImageView) rowView.findViewById(R.id.coins_image);

        txtTitle.setText(itemname[position]);
        imageView.setImageResource(imgid[position]);
        txtCoins.setText(itemname[position]);
        imageCoinsView.setImageResource(imgid[position]);
        return (rowView);

    };
}